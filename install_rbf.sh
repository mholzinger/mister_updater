#!/bin/bash

SD="/media/fat"

consolelist=($(ls ${SD}/_Console/*rbf |\
  while read -r files
    do basename "$files"|\
      cut -d '_' -f 1|\
      sed 's/\.rbf//g'
    done|sort -u))

computerlist=($(ls ${SD}/_Computer/*rbf |\
  while read -r files
    do basename "$files"|\
      cut -d '_' -f 1|\
      sed 's/\.rbf//g'
    done|sort -u))

# Install Arcade RBF files to ${SD}/_Arcade
files=(*)

for i in "${files[@]}"
do
  strip_ext=$(echo "$i"|cut -d '_' -f1|sed 's/.rbf//g')

  # Copy Arcade rbf files

  if [[ $i == *Arcade-*rbf ]]; then
    arcade_rbf=$(echo "$i"|sed 's/Arcade-//g')
    echo "Moving arcade file ["$arcade_rbf"] to [${SD}/_Arcade/]"
    mv "$i" ${SD}/_Arcade/"$arcade_rbf"
  fi

  # Console files
  if [[ "${consolelist[*]}" == *"$strip_ext"* ]]; then
    echo "Moving updated console file ["$i"] to [${SD}/_Console/]"
    mv "$i" ${SD}/_Console/"$i"
  fi

  # Computer files
  if [[ "${computerlist[*]}" == *"$strip_ext"* ]]; then
    echo "Moving updated computer file ["$i"] to [${SD}/_Computer/]"
    mv "$i" ${SD}/_Computer/"$i"
  fi

done
